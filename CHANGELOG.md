# Changelog

## 3.2.0 - 2025-11-25

### Added

- Installable en tant que package Composer
- spip/safehtml#4786 Ajout du Sanitizer SVG auparavant dans le plugin medias
- Fichier `README.md`

### Changed

- Utilisation du collecteur `HtmlTag` (refactor)
- Compatible SPIP 5.0.0-dev
- spip/spip#5271 Utilise HTMLPurifier à la place de SafeHTML
- Conversion des tests unitaires en PHPUnit
